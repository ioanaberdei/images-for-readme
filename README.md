# How to Gestapo Front
------------

Înainte să pornim efectiv frontul pentru Gestapo va trebui să facem **2 mici configurări** pentru a pregăti hostul pentru aplicația noastră.
**Așadar, numărul 1:** 
- Deschideți fișierul *hosts* pe care îl găsiți urmând o cale de genul: *C:\Windows\System32\drivers\etc* 
![](https://gitlab.com/ioanaberdei/images-for-readme/raw/master/1.png)

- Adăugați :
```127.0.0.1 		gestapo.wd ```
-- Ceea ce înseamnă că noi o să deschidem proiectul pe localhost


![](https://gitlab.com/ioanaberdei/images-for-readme/raw/master/2.png)

(Notă, pentru a putea salva trebuie să deschideți în mod* Administrator*)

------------

**Numărul 2:**
- Deschideți fișierul *httpd-vhosts.conf* pe care îl găsiți urmând o cale de genul:  *C:\xampp\apache\conf\extra* 
![](https://gitlab.com/ioanaberdei/images-for-readme/raw/master/3.png)
- Acolo adăugăm
![](https://gitlab.com/ioanaberdei/images-for-readme/raw/master/4.png)

-- Ceea ce înseamnă că vom folosi portul 80 pentru a deschide aplicația pe hostul pe care l-am definit mai devreme

**În final, în linia de comandă rulăm**
` npm start`

**Și gata**

![](https://gitlab.com/ioanaberdei/images-for-readme/raw/master/5.png)



